import sys
from pathlib import Path
from talerbuildconfig import *

b = BuildConfig()
b.enable_prefix()
b.enable_configmk()

# Base URL override for the site.  The default (empty string)
# will fall back to the base URL defined www.yml.
b.use(Option("baseurl", "Override base URL that the site will run on", default="", required=False))

b.use(PythonTool())
b.use(PyBabelTool())
b.use(PosixTool("cp"))
b.use(PosixTool("echo"))
b.use(PosixTool("env"))
b.use(PosixTool("printf"))
b.use(PosixTool("grep"))
b.use(PosixTool("mkdir"))
b.use(PosixTool("rm"))
b.use(PosixTool("sh"))
b.use(PosixTool("msgmerge"))
b.use(PosixTool("tsc"))
b.use(PosixTool("git"))
b.use(BrowserTool())
b.run()
