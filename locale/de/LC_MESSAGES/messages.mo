��          �               �   r   �   �   `  �        �  +   �  <     R   Z  x   �  I   &  c   p     �     �  �  �  r   s  �   �  �   �     X  +   w  <   �  R   �  x   3	  I   �	  c   �	     Z
     ^
   GNU Taler is developed as part of the <a href='https://www.gnu.org/'>GNU project</a> for the GNU operating system. If you do not have a Taler wallet installed, please first install the wallet from <a href="https://wallet.taler.net/">wallet installation page</a>. It only takes one click. In the GNU Taler system, an auditor is responsible for verifying that an exchange operates correctly. If you trust us to do a good job auditing, please scan the following QR code or open the link: JavaScript license information This is an auditor for the {curr} currency. This is the Web site of the auditor for the {curr} currency. This page was created using <a href='https://www.gnu.org/'>Free Software</a> only. This will tell your wallet that you are willing to trust exchanges that we audit to handle the {curr} currency properly. We are continuously publishing our <a href="/reports/">audit reports</a>. We are grateful for support and free hosting of this site by <a href='https://www.bfh.ch/'>BFH</a>. and {curr} Auditor Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2021-05-27 16:34+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: en
Language-Team: en <LL@li.org>
Plural-Forms: nplurals=2; plural=(n!=1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.6.0
 GNU Taler is developed as part of the <a href='https://www.gnu.org/'>GNU project</a> for the GNU operating system. If you do not have a Taler wallet installed, please first install the wallet from <a href="https://wallet.taler.net/">wallet installation page</a>. It only takes one click. In the GNU Taler system, an auditor is responsible for verifying that an exchange operates correctly. If you trust us to do a good job auditing, please scan the following QR code or open the link: JavaScript license information This is an auditor for the {curr} currency. This is the Web site of the auditor for the {curr} currency. This page was created using <a href='https://www.gnu.org/'>Free Software</a> only. This will tell your wallet that you are willing to trust exchanges that we audit to handle the {curr} currency properly. We are continuously publishing our <a href="/reports/">audit reports</a>. We are grateful for support and free hosting of this site by <a href='https://www.bfh.ch/'>BFH</a>. and {curr} Auditor 